package org.nrg.dicom.nifti2dcm;

import org.dcm4che3.data.*;
import org.dcm4che3.imageio.plugins.dcm.DicomImageReadParam;
import org.dcm4che3.imageio.plugins.dcm.DicomImageReader;
import org.dcm4che3.imageio.plugins.dcm.DicomMetaData;
import org.dcm4che3.io.DicomInputStream;
import org.dcm4che3.io.DicomOutputStream;
import org.junit.jupiter.api.Test;
import sun.jvmstat.perfdata.monitor.protocol.file.FileMonitoredVm;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.awt.image.Raster;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.ShortBuffer;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.fail;

public class TestUnsigned2Signed {

    @Test
    public void signed2Unsigned2() {
        try {

            ClassLoader loader = Nifti2Dcm.class.getClassLoader();
            File src = new File(loader.getResource("hepaticvessel_145-16bit.dcm").toURI());
            File dst = new File("/tmp/nifti/hepaticvessel_145-16bit-unsigned.dcm");

            DicomInputStream dis = new DicomInputStream( src);
            Attributes dataset = dis.readDataset( -1, -1);

            byte[] bytes = dataset.getSafeBytes( Tag.PixelData);

            ByteOrder bo = dataset.bigEndian()? ByteOrder.BIG_ENDIAN: ByteOrder.LITTLE_ENDIAN;
            ShortBuffer signedShorts = ByteBuffer.wrap(bytes).order( bo).asShortBuffer();

            int smin = Integer.MAX_VALUE;
            int smax = Integer.MIN_VALUE;
            for( int i = 0; i < signedShorts.capacity(); i++) {
                smin = (smin < signedShorts.get(i))? smin: signedShorts.get(i);
                smax = (smax > signedShorts.get(i))? smax: signedShorts.get(i);
            }

            for( int i = 0; i < signedShorts.capacity(); i++) {
                short sval = signedShorts.get(i);
                short uval = (short) (sval + Short.MIN_VALUE);
                signedShorts.put( i, uval);
            }

            int umin = Integer.MAX_VALUE;
            int umax = Integer.MIN_VALUE;
            for( int i = 0; i < signedShorts.capacity(); i++) {
                umin = (umin < signedShorts.get(i))? umin: signedShorts.get(i);
                umax = (umax > signedShorts.get(i))? umax: signedShorts.get(i);
            }

            dataset.setBytes(Tag.PixelData, VR.OW, bytes);

            ElementDictionary dictionary = StandardElementDictionary.getStandardElementDictionary();
            dataset.setInt( Tag.PixelRepresentation, dictionary.vrOf( Tag.PixelRepresentation), 0);
            dataset.setString( Tag.RescaleIntercept, dictionary.vrOf( Tag.RescaleIntercept), "0");
            dataset.setString( Tag.RescaleSlope, dictionary.vrOf( Tag.RescaleSlope), "1.0");
            dataset.setString( Tag.PatientName, dictionary.vrOf( Tag.PatientName), "Unsigned^Short");
            dataset.setString( Tag.PatientID, dictionary.vrOf( Tag.PatientID), "123");
            dataset.setString( Tag.StudyInstanceUID, dictionary.vrOf( Tag.StudyInstanceUID), "123");
            dataset.setString( Tag.SeriesInstanceUID, dictionary.vrOf( Tag.SeriesInstanceUID), "123.1");
            dataset.setString( Tag.SOPInstanceUID, dictionary.vrOf( Tag.SOPInstanceUID), "123.1.1");

            Attributes fmi = dataset.createFileMetaInformation( UID.ExplicitVRLittleEndian);

            DicomOutputStream dos = new DicomOutputStream( dst);
            dos.writeDataset( fmi, dataset);

            System.out.println("umin");

        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected exception: " + e);
        }
    }

    public void signed2Unsigned() {
        try {

            ClassLoader loader = Nifti2Dcm.class.getClassLoader();
            File src = new File(loader.getResource("hepaticvessel_145-16bit.dcm").toURI());
            File dst = new File("/tmp/nifti/hepaticvessel_145-16bit-unsigned.dcm");

            DicomImageReader reader = (DicomImageReader) ImageIO.getImageReadersByFormatName("DICOM").next();
            DicomInputStream dicomStream = new DicomInputStream( src);
            reader.setInput(dicomStream);

            Raster raster = reader.readRaster( 0, new DicomImageReadParam());

            int size = raster.getDataBuffer().getSize();
            DataBuffer dataBuffer;
            if( raster.getDataBuffer() instanceof DataBufferUShort) {
                dataBuffer = new DataBufferShort(((DataBufferUShort) raster.getDataBuffer()).getData(), size);
            }
            else {
                dataBuffer = raster.getDataBuffer();
            }

            int smin = Integer.MAX_VALUE;
            int smax = Integer.MIN_VALUE;
            for( int i = 0; i < size; i++) {
                smin = (smin < dataBuffer.getElem(i))? smin: dataBuffer.getElem(i);
                smax = (smax > dataBuffer.getElem(i))? smax: dataBuffer.getElem(i);
            }

            DataBufferUShort dataBufferUShort = new DataBufferUShort( size);
            for( int i = 0; i < size; i++) {
                int sval = dataBuffer.getElem(i);
                int uval = sval + Short.MIN_VALUE;
                dataBufferUShort.setElem( i, uval);
            }

            int umin = Integer.MAX_VALUE;
            int umax = Integer.MIN_VALUE;
            for( int i = 0; i < size; i++) {
                umin = (umin < dataBufferUShort.getElem(i))? umin: dataBufferUShort.getElem(i);
                umax = (umax > dataBufferUShort.getElem(i))? umax: dataBufferUShort.getElem(i);
            }

            DicomMetaData dicomMetaData = reader.getStreamMetadata();
            Attributes attributes = dicomMetaData.getAttributes();
            byte[] bytes = new byte[size * 2];
            ByteOrder bo = attributes.bigEndian()? ByteOrder.BIG_ENDIAN: ByteOrder.LITTLE_ENDIAN;
            ByteBuffer.wrap(bytes).order( bo).asShortBuffer().put( dataBufferUShort.getData());
            attributes.setBytes(Tag.PixelData, VR.OW, bytes);

            ElementDictionary dictionary = StandardElementDictionary.getStandardElementDictionary();
            attributes.setInt( Tag.PixelRepresentation, dictionary.vrOf( Tag.PixelRepresentation), 0);
            attributes.setString( Tag.RescaleIntercept, dictionary.vrOf( Tag.RescaleIntercept), "0");
            attributes.setString( Tag.RescaleSlope, dictionary.vrOf( Tag.RescaleSlope), "1.0");
            DicomOutputStream dos = new DicomOutputStream( dst);
            dos.writeDataset( null, attributes);

            System.out.println("umin");
//            }

//                DicomObjectI dobj = DicomObjectFactory.newInstance( src);
//
//                JsonRectanglePixelEditHandler eh = new JsonRectanglePixelEditHandler();
////                eh.apply( jsonString, null, null, null, dobj);
//                if( ! dst.getParentFile().exists()) Files.createDirectories( dst.getParentFile().toPath());
//                dobj.write( new FileOutputStream( dst));
        } catch (Exception e) {
            e.printStackTrace();
            fail("Unexpected exception: " + e);
        }
    }
}
