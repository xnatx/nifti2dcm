package org.nrg.dicom.nifti2dcm;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class Nifti2DcmTest {

    @Test
    void processFile() {
        Nifti2Dcm nifti2Dcm = new Nifti2Dcm();
        try {
            nifti2Dcm.process("/Users/drm/projects/nrg/nifti2dcm/src/main/resources/hepaticvessel_145.nii", "/tmp/dcm");
        } catch (IOException e) {
            fail("Unexpected exception: " + e);
        }
    }

    @Test
    void processDirTr() {
        Nifti2Dcm nifti2Dcm = new Nifti2Dcm();
        try {
            nifti2Dcm.process("/Users/drm/Box/data/machineLearning/Task08_HepaticVessel/flipped-16bit-nifti/imagesTr", "/Users/drm/Box/data/machineLearning/Task08_HepaticVessel/dcm-16bit-flipped/imagesTr");
        } catch (IOException e) {
            fail( "Unexpected exception: " + e);
        }
    }

    @Test
    void processDir() {
        Nifti2Dcm nifti2Dcm = new Nifti2Dcm();
        try {
            nifti2Dcm.process("/Users/drm/Box/data/machineLearning/Task08_HepaticVessel/flipped-16bit-nifti/imagesTs", "/Users/drm/Box/data/machineLearning/Task08_HepaticVessel/dcm-16bit-flipped/imagesTs");
        } catch (IOException e) {
            fail( "Unexpected exception: " + e);
        }
    }

    @Test
    void processDirLabelTr() {
        Nifti2Dcm nifti2Dcm = new Nifti2Dcm();
        try {
            nifti2Dcm.process("/Users/drm/Box/data/machineLearning/Task08_HepaticVessel/flipped-16bit-nifti/labelsTr", "/Users/drm/Box/data/machineLearning/Task08_HepaticVessel/dcm-16bit-flipped/labelsTr");
        } catch (IOException e) {
            fail( "Unexpected exception: " + e);
        }
    }

    @Test
    void processAbCTTrain() {
        Nifti2DcmAbCTImages nifti2Dcm = new Nifti2DcmAbCTImages();
        try {
            nifti2Dcm.process("/Users/drm/Box/data/machineLearning/abdominalCT-seg/Abdomen/RawData/Training/img-flipped-nifti", "/Users/drm/Box/data/machineLearning/abdominalCT-seg/Abdomen/RawData/Training/img.dcm");
        } catch (IOException e) {
            fail( "Unexpected exception: " + e);
        }
    }

    @Test
    void processAbCTLabel() {
        Nifti2DcmAbCTLabels nifti2Dcm = new Nifti2DcmAbCTLabels();
        try {
            nifti2Dcm.process("/Users/drm/Box/data/machineLearning/abdominalCT-seg/Abdomen/RawData/Training/label-flipped-nifti", "/Users/drm/Box/data/machineLearning/abdominalCT-seg/Abdomen/RawData/Training/label.dcm");
        } catch (IOException e) {
            fail( "Unexpected exception: " + e);
        }
    }
    @Test
    void processAbCTTest() {
        Nifti2DcmAbCTImages nifti2Dcm = new Nifti2DcmAbCTImages();
        try {
            nifti2Dcm.process("/Users/drm/Box/data/machineLearning/abdominalCT-seg/orig-flipped/test", "/Users/drm/Box/data/machineLearning/abdominalCT-seg/dcm/test");
        } catch (IOException e) {
            fail( "Unexpected exception: " + e);
        }
    }

    @Test
    void processPancreasTrain() {
        Nifti2DcmAbCTLabels nifti2Dcm = new Nifti2DcmAbCTLabels();
        try {
            nifti2Dcm.process("/Users/drm/Box/data/machineLearning/pancreas/orig.flipped/labels", "/Users/drm/Box/data/machineLearning/pancreas/labels-n2d");
        } catch (IOException e) {
            fail( "Unexpected exception: " + e);
        }
    }

    @Test
    void processSpleenImages() {
        Nifti2DcmSpleenImages nifti2Dcm = new Nifti2DcmSpleenImages();
        try {
            nifti2Dcm.process("/Users/drm/Box/DataSets/Spleen/rotated/imagesTr", "/Users/drm/Box/DataSets/Spleen/dicom/imagesTr");
            nifti2Dcm.process("/Users/drm/Box/DataSets/Spleen/rotated/imagesTs", "/Users/drm/Box/DataSets/Spleen/dicom/imagesTs");
        } catch (IOException e) {
            fail( "Unexpected exception: " + e);
        }
    }

    @Test
    void processSpleenLabels() {
        Nifti2DcmSpleenLabels nifti2Dcm = new Nifti2DcmSpleenLabels();
        try {
            nifti2Dcm.process("/Users/drm/Box/DataSets/Spleen/rotated/labelsTr", "/Users/drm/Box/DataSets/Spleen/dicom/labelsTr");
        } catch (IOException e) {
            fail( "Unexpected exception: " + e);
        }
    }

    @Test
    void processCovid19() {
        Nifti2DcmCovid19Images nifti2Dcm = new Nifti2DcmCovid19Images();
        try {
            nifti2Dcm.process("/Users/drm/Box/DataSets/Covid-19/converted/rp_img", "/Users/drm/Box/DataSets/Covid-19/dicom/rp_img");
        } catch (IOException e) {
            fail( "Unexpected exception: " + e);
        }
        Nifti2DcmCovid19Labels nifti2DcmCovid19Labels = new Nifti2DcmCovid19Labels();
        try {
            nifti2DcmCovid19Labels.process("/Users/drm/Box/DataSets/Covid-19/converted/rp_msk", "/Users/drm/Box/DataSets/Covid-19/dicom/rp_msk");
        } catch (IOException e) {
            fail( "Unexpected exception: " + e);
        }
        Nifti2DcmCovid19LungLabels nifti2DcmCovid19LungLabels = new Nifti2DcmCovid19LungLabels();
        try {
            nifti2DcmCovid19LungLabels.process("/Users/drm/Box/DataSets/Covid-19/converted/rp_lung_msk", "/Users/drm/Box/DataSets/Covid-19/dicom/rp_lung_msk");
        } catch (IOException e) {
            fail( "Unexpected exception: " + e);
        }
    }

}