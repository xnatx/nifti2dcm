package org.nrg.dicom.nifti2dcm;

import com.pixelmed.convert.NIfTI1Exception;
import com.pixelmed.convert.NIfTI1ToDicom;
import com.pixelmed.dicom.DicomException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.GZIPInputStream;

public class Nifti2DcmSpleenImages {

    public static void main(String[] args) {

        Nifti2DcmSpleenImages nifti2Dcm = new Nifti2DcmSpleenImages();
        try {
            nifti2Dcm.process( Paths.get(args[0]), Paths.get(args[1]));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void process( String inString, String outString) throws IOException {
        process( Paths.get(inString), Paths.get(outString));
    }

    public void process(Path inPath, Path outPath) throws IOException  {
        if( inPath.toFile().isDirectory()) {
            for( File f: inPath.toFile().listFiles()) {
                if( f.isFile() ) {
                    processPath( f, outPath);
                }
            }
        }
        else {
            processPath( inPath.toFile(), outPath);
        }
    }

    public void processPath( File inFile, Path outPathRoot) throws IOException {
        String inFilename = inFile.getName();
        String inFilenameRoot;
        Path outFilePath;
        if( inFilename.endsWith(".gz")) {
            File tmpFile = gunzip( inFile);
            inFilenameRoot = inFilename.substring(0, inFilename.lastIndexOf("."));
            inFilenameRoot = inFilenameRoot.substring(0, inFilenameRoot.lastIndexOf("."));
            String id = getID( inFilenameRoot);
            outFilePath = outPathRoot.resolve( inFilenameRoot + ".dcm");
            if (!outPathRoot.toFile().exists())
                Files.createDirectories(outPathRoot);
            processFile( tmpFile, id, outFilePath.toFile());
            tmpFile.delete();
        }
        else {
            inFilenameRoot = inFilename.substring(0, inFilename.lastIndexOf("."));
            String id = getID( inFilenameRoot);
            outFilePath = outPathRoot.resolve(inFilenameRoot + ".dcm");
            if (!outPathRoot.toFile().exists())
                Files.createDirectories(outPathRoot);
            processFile(inFile, id, outFilePath.toFile());
        }
    }

    public String getID( String inFileNameRoot) {
        return inFileNameRoot.split("_")[1];
    }

    public void processFile1( File inFile, String id, File outFile) {
        System.out.println("inFile: " + inFile + "  id: " + id + "  outFile: " + outFile);
    }

    public void processFile( File inFile, String id, File outFile) {
        String inputFileName = inFile.getAbsolutePath();
        String outputFileName = outFile.getAbsolutePath();
        String patientName = id + "^Spleen";
        String patientID = id+"_spleen";
//        String patientID = id;
        String studyID = "1";
        String seriesNumber = "1";
        String instanceNumber = "1";
        String modality = "CT";
        String sopClass = "1.2.840.10008.5.1.4.1.1.2.1";

        try {
            System.out.println("inFile: " + inFile + "  id: " + id + "  outFile: " + outFile);
            Files.createDirectories(Paths.get(outputFileName).getParent());
            NIfTI1ToDicom n2d = new NIfTI1ToDicom(inputFileName, outputFileName, patientName, patientID, studyID, seriesNumber, instanceNumber, modality, sopClass);
        } catch (IOException | DicomException | NIfTI1Exception e) {
            e.printStackTrace();
        }
    }

//    public void flipPM(File f) throws IOException, DicomException {
//        AttributeList attributeList = new AttributeList();
//        attributeList.read( f);
//        SourceImage sourceImage = new SourceImage( attributeList);
//        ImageEditUtilities.rotateAndFlip( sourceImage, attributeList, 0, true);
//        attributeList.write(f, TransferSyntax.ExplicitVRLittleEndian,true,true);
//    }
//
//    public void flip(File f) throws IOException, DicomException {
//        ImagePlus ip = IJ.openImage( f.getAbsolutePath());
//        ImageProcessor iproc = ip.getProcessor();
//        iproc.flipVertical();
//
//        IJ.saveAs(ip, "nifti-1", "/tmp/dcm/foo.nii");
//    }

    public File gunzip(File f) throws IOException {

        byte[] buffer = new byte[1024];

        try( GZIPInputStream gzis = new GZIPInputStream(new FileInputStream(f))){

            File tmpOutFile = File.createTempFile("tmp", "");
            try( FileOutputStream out = new FileOutputStream(tmpOutFile)) {
                int len;
                while ((len = gzis.read(buffer)) > 0) {
                    out.write(buffer, 0, len);
                }
                return tmpOutFile;
            }
        }
    }

}
