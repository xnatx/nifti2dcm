#Nifti2Dcm

Rudimentary code to convert Nifti to Dicom. Much is hardcoded and individual runs are test cases
in src/test/java/org/nrg/dicom/nifti2dcm/Nifti2DcmTest.java. This runs pixelmed's nifti 
converter and adds minimal DICOM metadata.